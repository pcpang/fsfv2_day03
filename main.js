/**
 * Created by pohchye on 29/6/2016.
 */

//Load Express - look inside node_modules for the module
var express = require("express");
// var math = require('mathjs');

// Create an instance of express app
var app = express();

// --- Start of request processing ---

// This will be executed every time and then pass to next Middleware using next
// app.use(function (req, res, next) {
//     console.info("incoming request: %s", req.originalUrl);
//     next();
// });

// Process all Get request
app.use(function (req, res, next) {
    var currTime =  new Date();
    res.status(200);
    res.type('text/html');
    // var param1 = req.get('Method');
    var param1 = req.method;
    var param2 = req.originalUrl;
    //res.send("Method: " + param1 + " Path: "+ param2);
    if ((param1 == "GET") && (param2 == "/time")) {
        res.send("<h1>Using Use, The current time is: " + currTime + "</h1>");
    } else {
        //res.send("<h1>Not working</h1>");
        next();
    }
});

// Process Request
app.get("/time", function (req, res) {
    var currTime =  new Date();
    res.status(200);
    // res.type('text/plain');
    res.type('text/html');
    // res.set("My-Header", "Hello");
    // res.send("The current time is: " + currTime);
    // res.send("<html><body><h1>The current time is: " + currTime + "</h1></body></html>");
    res.send("<h1>The current time is: " + currTime + "</h1>");

});

app.get("/img", function (req, res) {
    res.status(200);
    // res.type('text/html');
    res.type('text/html');
    // var number = 1;
    var pictures = ["pic1.jpg", "pic2.jpg", "pic3.jpg", "pic4.jpg"];
    var num1 = Math.floor((Math.random() * pictures.length) + 1);
    res.send("<img src='images/" + pictures[num1-1] + "'>" + "<h1>" + pictures[num1-1] + "</h1>");

    // var num1 = Math.floor((Math.random() * 4) + 1);
    // res.send("<html><body><img src='images/pic" + number + ".jpg></body></html>");
    // res.send("<img src='images/pic" + num1 + ".jpg'>");


});

// Serve static files from public
app.use(express.static(__dirname + "/public"));
// Serve files from another directory bower_components
app.use(express.static(__dirname + "/bower_components"));
// Method: Get Resource: /hello
// app.get('/hello', function (req, res) {
//     res.send('hello everyone');
// });
// app.get('/hello', function (req, res) {
//     res.render('hello');
// });

// Execute this Middleware when the above is not successful
app.use(function (req, res) {
    console.info("Fle not found in public");
    res.redirect("error.html");
});


// Set node port
// Check for 1st argument, environment variable and then hard code port
app.set("port",
    process.argv[2] || process.env.APP_PORT || 3003);

//Start server on port
app.listen(app.get("port"), function(){
        console.info("Webserver started on port %d", app.get("port"));
    }
);



